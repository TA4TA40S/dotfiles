# Dotfiles

Here are my dotfiles.

Window Manager: i3 Gaps

Fonts needed:
* FontAwesome(Icons)
* Space Monofor Powerline (Applications)
* Fira Code (Emacs)
* AirStream NF (Emacs)

Gtk Theme: Oomox custom 

Icons: Oomox custom

Wallpaper: https://alpha.wallhaven.cc/wallpaper/363035 with nitrogen

Ide: Emacs

Web browser: IceCat

File manager: PCManFM && Ranger

Bar: Polybar

Dmenu: Rofi

Other software: 
* Redshift 
* Discord
* Gitkraken
* Virtualbox
* Dolphin emulator
